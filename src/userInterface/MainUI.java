package userInterface;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import exportAnnuairePdf.EventExportAnnuairePdf;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import methodGetAll.Stagiaire;
import methodGetAll.StagiaireGetAll;
import methodGetAll.TablePannel;
import methodImport.Import;

public class MainUI {

	public static ObservableList<Stagiaire> stagiaires = FXCollections.observableArrayList(StagiaireGetAll.getAll());
	public static TableView<Stagiaire> tp = TablePannel.makeTableView(stagiaires);

	public static TextField tf2 = new TextField();
	public static TextField tf1 = new TextField();
	public static TextField tf3 = new TextField();
	public static TextField tf4 = new TextField();
	public static TextField tf5 = new TextField();
	public static AnchorPane rightPanel = new AnchorPane();
	public static AnchorPane leftPanel = new AnchorPane();
	public static List<TextField> tfs = new ArrayList<TextField>();

	protected static Scene sceneMainUI;
	protected static Button btn3;
	protected static Button btn4;
	protected static Button admin;

	protected static Label lbl1;
	protected static Label lbl2;
	protected static Label lbl3;
	protected static Label lbl4;
	protected static Label lbl5;

	@SuppressWarnings("static-access")
	public static Scene mainPanel() {

		Collections.addAll(tfs,tf1,tf2,tf3,tf4,tf5);

		//FRONT
		//root panel (TOUTE l'interface utilisateur)
		HBox root = new HBox();
		//rightPanel (contains Table)
		rightPanel.setStyle("-fx-background-color:olive");
		//leftPanel (contains user controls)

		VBox vBox = new VBox(40);

		//helpAdmin
		HBox helpAdmin = new HBox(40);

		Button help = new Button("Aide");
		help.setPrefSize(80, 40);

		admin = new Button("Admin");
		admin.setPrefSize(80, 40);

		helpAdmin.getChildren().addAll(help,admin);
		helpAdmin.setAlignment(Pos.CENTER_RIGHT);

		//LOGO
		ImageView img = new ImageView("./Bin/eql.png");
		img.setFitWidth(350);
		img.setPreserveRatio(true);
		img.setSmooth(true);
		img.setCache(true);

		//topBox (export, aide, admin)
		VBox topBox = new VBox();

		Button export = new Button("Export");
		export.setPrefSize(80,30);

		topBox.getChildren().addAll(export,helpAdmin);

		//searchStagiaires
		GridPane searchStagiaires = new GridPane();

		lbl1 = new Label("Nom");
		tf1.setPrefWidth(450);	// adjust to avoid labels getting squashed
		lbl2 = new Label("Pr�nom");
		lbl3 = new Label("D�partement");
		lbl4 = new Label("Promo");
		lbl5 = new Label("Ann�e");

		searchStagiaires.addRow(0, lbl1, tf1);
		searchStagiaires.addRow(1, lbl2, tf2);
		searchStagiaires.addRow(2, lbl3, tf3);
		searchStagiaires.addRow(3, lbl4, tf4 );
		searchStagiaires.addRow(4, lbl5, tf5 );

		searchStagiaires.setVgap(20);
		searchStagiaires.setHgap(20);

		searchStagiaires.setAlignment(Pos.CENTER_RIGHT);
		searchStagiaires.setHgrow(lbl1, Priority.ALWAYS);

		//crudBox
		HBox crudBox = new HBox(40);

		Button btn1 = new Button("Recherche");
		btn1.setPrefSize(110, 40);

		Button btn2 = new Button("Ajout");
		btn2.setPrefSize(110, 40);

		btn3 = new Button("Suppression");
		btn3.setPrefSize(110, 40);
		btn3.setVisible(false); 

		btn4 = new Button("Mise-�-jour");
		btn4.setPrefSize(110, 40);
		btn4.setVisible(false); 

		crudBox.getChildren().addAll(btn1,btn2,btn3,btn4); //del btn3,btn4 pour visualiser en tant qu'utilisateur
		crudBox.setAlignment(Pos.CENTER_LEFT);

		//fileImpExp,helpAdmin,searchStagiaires et crudBox constituent la vBOX
		vBox.getChildren().addAll(img,topBox,searchStagiaires,crudBox);
		vBox.setAlignment(Pos.CENTER);

		vBox.setPadding(new Insets(40));
		vBox.setAlignment(Pos.CENTER);

		//MANAGE LEFT AND RIGHT
		leftPanel.getChildren().add(vBox);
		AnchorPane.setTopAnchor(vBox, 1.);
		AnchorPane.setBottomAnchor(vBox, 1.);
		AnchorPane.setLeftAnchor(vBox, 1.);
		AnchorPane.setRightAnchor(vBox, 1.);

		rightPanel.getChildren().add(tp);
		AnchorPane.setTopAnchor(tp, 1.);
		AnchorPane.setBottomAnchor(tp, 1.);
		AnchorPane.setLeftAnchor(tp, 1.);
		AnchorPane.setRightAnchor(tp, 1.);

		//MANAGE ROOT
		root.getChildren().addAll(leftPanel,rightPanel);
		root.setHgrow(rightPanel, Priority.ALWAYS);
		//

		//FRONTEND TEST
		if (MainApp.theme.equals(MainApp.b)) {
			leftPanel.setStyle("-fx-background-image: url("+MainApp.theme+")");
			darkTheme();
		} else if (MainApp.theme.equals(MainApp.a)) {
			leftPanel.setStyle("-fx-background-image: url("+MainApp.theme+")");
		} else leftPanel.setStyle("-fx-background-color:linear-gradient(#0080c0, #ffffff)");

		//


		tp.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Stagiaire>() {
			@Override
			public void changed(ObservableValue<? extends Stagiaire> observable, Stagiaire oldValue,
					Stagiaire newValue) {
				tf1.setText(newValue.getLastName());
				tf2.setText(newValue.getFirstName());
				tf3.setText(newValue.getDepartment());
				tf4.setText(newValue.getPromoName());
				tf5.setText(newValue.getYear());
			}
		});

		//BUTTONS
		//HELP
		help.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				File help = new File(MainApp.relativepath + "/Bin/help.txt");
				try {
					Desktop.getDesktop().open(help);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});



		//RECHERCHE
		btn1.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (verifField(tfs)) {
					List<Stagiaire> stagiaires2 = StagiaireGetAll.getAll();
					for (int i = 0;i<5;i++) {
						ListIterator<Stagiaire> it = stagiaires2.listIterator();
						if(!(tfs.get(i).getText().trim().isEmpty())) {
							while(it.hasNext()) {
								if (!(it.next().toString().substring((i*30), ((i+1)*30)).toUpperCase().contains(tfs.get(i).getText().toUpperCase().trim()))) { 
									it.remove();
								} 
							}
						}
					}

					stagiaires = FXCollections.observableArrayList(stagiaires2);
					for (TextField tf : tfs) tf.clear();
				}
				majTablePanel();



			}
		});

		//AJOUT
		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					if (verifField(tfs)) {
						Import im = new Import();
						im.createStagiaire(tfs);
						for (TextField tf : tfs) tf.clear();
						stagiaires = FXCollections.observableArrayList(StagiaireGetAll.getAll());
					}
					majTablePanel();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		//SUPPRIMER
		btn3.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					if (verifField(tfs)) {
						Import im = new Import();
						String toDelete="";
						for (TextField tf : tfs) {
							String space = "";
							for(int i =0;i<(30-(new String (tf.getText().getBytes(),StandardCharsets.UTF_8).trim().length()));i++) space +=" ";
							toDelete+= new String (tf.getText().getBytes(),StandardCharsets.UTF_8).trim()+space;
						}
						im.searchNodePositionUtil(toDelete);
						if (im.posDepart==-1){
							for (TextField tf : tfs) tf.clear();
							tfs.get(0).setText("Stagiaire introuvable, aucun �l�ment supprim�");
							im.posDepart=0;
						}else {
							im.deleteNode(toDelete);
							stagiaires = FXCollections.observableArrayList(StagiaireGetAll.getAll());
							for (TextField tf : tfs) tf.clear();
							tfs.get(0).setText("Le stagiaire "+toDelete.substring(30, 60).trim()+" "+toDelete.substring(0, 30).trim()+" � �t� supprim� de la base");
						}

					}
					majTablePanel();	

				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		});

		//EXPORT
		export.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (verifField(tfs)) {
					File expFolder = new File (MainApp.relativepath + "/Workspace_Projet1/Export");
					EventExportAnnuairePdf.exportPdf(stagiaires);
					try {
						Desktop.getDesktop().open(expFolder);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});

		sceneMainUI = new Scene(root);
		return sceneMainUI;
	}
	public static void majTablePanel() {

		tp = TablePannel.makeTableView(stagiaires);
		rightPanel.getChildren().remove(0);
		rightPanel.getChildren().add(tp);
		AnchorPane.setTopAnchor(tp, 1.);
		AnchorPane.setBottomAnchor(tp, 1.);
		AnchorPane.setLeftAnchor(tp, 1.);
		AnchorPane.setRightAnchor(tp, 1.);

		tp.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Stagiaire>() {

			@Override
			public void changed(ObservableValue<? extends Stagiaire> observable, Stagiaire oldValue,
					Stagiaire newValue) {
				tf1.setText(newValue.getLastName());
				tf2.setText(newValue.getFirstName());
				tf3.setText(newValue.getDepartment());
				tf4.setText(newValue.getPromoName());
				tf5.setText(newValue.getYear());
			}
		});
	}


	//V�rifier que les textfields ne contiennent pas de caract�res sp�ciaux ou soient trop long
	public static Boolean verifField (List<TextField> textfields) {

		boolean bool = true;
		for (TextField tf : textfields) {
			if (tf.getText().length()>25) {
				tf.setText("Saisie incorrecte, merci de ne pas d�passer 25 caract�res.");
				bool = false;

			}else if (tf.getText().contains("/") || tf.getText().contains("*") || tf.getText().contains(";")|| tf.getText().contains(",")) {
				tf.setText("Saisie incorrecte, merci de ne pas utiliser de caract�res sp�ciaux.");
				bool = false;
			}


		}
		return bool;
	}
	public static void darkTheme() {
		DropShadow shadow = new DropShadow();
		shadow.setRadius(60);
		shadow.setSpread(.85);
		lbl1.setEffect(shadow);
		lbl2.setEffect(shadow);
		lbl3.setEffect(shadow);
		lbl4.setEffect(shadow);
		lbl5.setEffect(shadow);
		lbl1.setTextFill(Color.WHITE);
		lbl2.setTextFill(Color.WHITE);
		lbl3.setTextFill(Color.WHITE);
		lbl4.setTextFill(Color.WHITE);
		lbl5.setTextFill(Color.WHITE);
	}

}