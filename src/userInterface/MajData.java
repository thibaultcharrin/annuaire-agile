package userInterface;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import methodGetAll.StagiaireGetAll;
import methodImport.Import;

public class MajData {

	public static Import im = new Import();
	public static String toModify="";
	public static Scene maj;

	protected static Label errorLabel;
	protected static Label lbl1;
	protected static Label lbl2;
	protected static Label lbl3;
	protected static Label lbl4;
	protected static Label lbl5;


	public static Scene Maj (String fName,String lName,String dpt, String pName, String year ) {

		//searchStagiaires
		TextField tf2 = new TextField();
		TextField tf1 = new TextField();
		TextField tf3 = new TextField();
		TextField tf4 = new TextField();
		TextField tf5 = new TextField();
		List<TextField> tfs = new ArrayList<TextField>();
		Collections.addAll(tfs,tf1,tf2,tf3,tf4,tf5);


		VBox errorPane = new VBox();
		Label errorLabel = new Label("Le stagiaire entr� n'est pas pr�sent dans la base, vous ne pouvez donc pas le modifier.\r\nMerci de selectionner un stagiaire dans le tableau.");
		errorPane.setPadding(new Insets(20));




		errorPane.getChildren().add(errorLabel);

		GridPane searchStagiaires = new GridPane();

		lbl1 = new Label("Nom");
		tf1.setPrefWidth(450);    // adjust to avoid labels getting squashed
		lbl2 = new Label("Pr�nom");
		lbl3 = new Label("D�partement");
		lbl4 = new Label("Promo");
		lbl5 = new Label("Ann�e");
		Button btnModifier = new Button("Mettre � jour");

		tf1.setText(fName);
		tf2.setText(lName);
		tf3.setText(dpt);
		tf4.setText(pName);
		tf5.setText(year);


		searchStagiaires.addRow(0, lbl1, tf1);
		searchStagiaires.addRow(1, lbl2, tf2);
		searchStagiaires.addRow(2, lbl3, tf3);
		searchStagiaires.addRow(3, lbl4, tf4 );
		searchStagiaires.addRow(4, lbl5, tf5 );
		searchStagiaires.addRow(5,btnModifier );

		searchStagiaires.setVgap(20);
		searchStagiaires.setHgap(20);

		searchStagiaires.setAlignment(Pos.CENTER_RIGHT);
		searchStagiaires.setPadding(new Insets(20));		// Thibault: mise en forme
		//searchStagiaires.setHgrow(lbl1, Priority.ALWAYS);

		//FRONTEND TEST
		if (MainApp.theme.equals(MainApp.b)) {
			searchStagiaires.setStyle("-fx-background-image: url("+MainApp.theme+")");
			darkTheme();
		} else if (MainApp.theme.equals(MainApp.a)) {
			searchStagiaires.setStyle("-fx-background-image: url("+MainApp.theme+")");
		} else searchStagiaires.setStyle("-fx-background-color:linear-gradient(#0080c0, #ffffff)");

		//

		maj = new Scene(searchStagiaires);



		for (TextField tf : tfs) {
			String space = "";
			for(int i =0;i<(30-(new String (tf.getText().getBytes(),StandardCharsets.UTF_8).trim().length()));i++) space +=" ";
			toModify+= new String (tf.getText().getBytes(),StandardCharsets.UTF_8).trim()+space;

		}
		try {
			im.searchNodePositionUtil(toModify);
			if (im.posDepart ==-1) {
				maj = new Scene(errorPane);
				im.posDepart=0;
				toModify="";
			}

			im.posDepart=0;

		} catch (IOException e1) {};

		btnModifier.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				try {
					im.posDepart=0;
					im.deleteNode(toModify);
					tfs.clear();
					Collections.addAll(tfs,tf1,tf2,tf3,tf4,tf5);
					im.createStagiaire(tfs);
					String modif ="";
					for (TextField tf : tfs) modif = modif+tf.getText()+" ";
					VBox confirmbox = new VBox();
					Label confirmLabel = new Label("le stagiaire : \r\n"+fName+" "+lName+" "+dpt+" "+pName+" "+year+"\r\na �t� mis � jour en :\r\n"+modif);
					confirmbox.getChildren().add(confirmLabel);
					confirmbox.setPadding(new Insets(20));
					confirmbox.setAlignment(Pos.CENTER);
					confirmbox.setPrefSize(350, 150);
					maj = new Scene(confirmbox);
					toModify="";
					MainUI.stagiaires = FXCollections.observableArrayList(StagiaireGetAll.getAll());
					MainUI.majTablePanel();
					MainApp.closeAdminApp();
					MainApp.showApp(maj);


				} catch (IOException e) {};


			}
		});


		return maj;


	}
	public static void darkTheme() {
		DropShadow shadow = new DropShadow();
		shadow.setRadius(60);
		shadow.setSpread(.85);
		lbl1.setEffect(shadow);
		lbl2.setEffect(shadow);
		lbl3.setEffect(shadow);
		lbl4.setEffect(shadow);
		lbl5.setEffect(shadow);
		lbl1.setTextFill(Color.WHITE);
		lbl2.setTextFill(Color.WHITE);
		lbl3.setTextFill(Color.WHITE);
		lbl4.setTextFill(Color.WHITE);
		lbl5.setTextFill(Color.WHITE);
	}
}