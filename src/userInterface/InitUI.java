package userInterface;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class InitUI {

	protected static Scene sceneInitUI;
	protected static Button valid;
	protected static PasswordField pwTf;
	
	protected static Label noBdd;
	protected static Label welcomeMsg;
	protected static Label fileMsg;
	protected static Label pwMsg;	

	public static Scene initApp() {

		/*
		 * On g�n�re d'abord un workspace utilisateur avec une arborescence.
		 * 'dir' correspond � l'emplacement du fichier 'stagiaires.txt' et
		 * la raf g�n�r�e par le programmer.
		 * 'dirBin' correspond � l'emplacement du fichier de persistance pour
		 * le mot de passe administrateur.
		 */
		
		File dir = new File(MainApp.relativepath + "/Workspace_Projet1/Source");
		File dirExp = new File(MainApp.relativepath + "/Workspace_Projet1/Export");
		File dirBin = new File(MainApp.relativepath + "/Bin");
		if (!dir.exists()) dir.mkdirs();
		if (!dirBin.exists()) dirBin.mkdir();
		if (!dirExp.exists()) dirExp.mkdir();

		/*
		 * Le fichier 'acs' sans extension contient le mot de passe administrateur.
		 */
		try {
			MainApp.passFile.createNewFile();		//FACTORISATION POSSIBLE?
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			Desktop.getDesktop().open(dir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Front
		//root
		BorderPane root = new BorderPane();

		VBox elements = new VBox(30);

		welcomeMsg = new Label("Bienvenue dans votre Gestionnaire de stagiaires");
		welcomeMsg.setFont(Font.font("Verdana", FontWeight.LIGHT, 20));

		fileMsg = new Label("Veuillez placer votre fichier stagiaires.txt"
				+ "\ndans le dossier Workspace_Projet1/Source");
		fileMsg.setFont(Font.font("Century Gothic", 18));

		pwMsg = new Label("Veuillez saisir un mot de passe administrateur : ");
		pwMsg.setFont(Font.font("Century Gothic", 18));

		HBox pwBox = new HBox(30);
		pwTf = new PasswordField();
		valid = new Button("Valider");
		noBdd = new Label("");
		pwBox.getChildren().addAll(pwTf,valid,noBdd);

		elements.getChildren().addAll(welcomeMsg,fileMsg,pwMsg,pwBox);
		elements.setPadding(new Insets(30));
		elements.setAlignment(Pos.CENTER_LEFT);

		root.setCenter(elements);

		//FRONTEND TEST
		if (MainApp.theme.equals(MainApp.b)) {
			root.setStyle("-fx-background-image: url("+MainApp.theme+")");
			darkTheme();
		} else if (MainApp.theme.equals(MainApp.a)) {
			root.setStyle("-fx-background-image: url("+MainApp.theme+")");
		} else root.setStyle("-fx-background-color:lightblue");
		
		root.setPrefSize(550, 300);

		sceneInitUI = new Scene(root);
		return sceneInitUI;
		//
	}
	public static void darkTheme() {
		DropShadow shadow = new DropShadow();
		shadow.setRadius(60);
		shadow.setSpread(.85);
		welcomeMsg.setEffect(shadow);
		fileMsg.setEffect(shadow);
		pwMsg.setEffect(shadow);
		noBdd.setEffect(shadow);
		welcomeMsg.setTextFill(Color.WHITE);
		fileMsg.setTextFill(Color.WHITE);
		pwMsg.setTextFill(Color.WHITE);
		noBdd.setTextFill(Color.WHITE);
	}
}
