package methodGetAll;

import java.util.ArrayList;
import java.util.List;

import methodImport.Import;



public class StagiaireGetAll {
	//
	
	//public static List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();

public static List<Stagiaire>  getAll() {
	List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();
	stagiaires = getAllNative(0,stagiaires);
	return stagiaires;
}

public static List<Stagiaire>  getAllNative(int pos,List<Stagiaire> stagiaires) {
	Import im = new Import();
	
	if (im.getBrancheGauche(pos) != -1) getAllNative(im.getBrancheGauche(pos),stagiaires);
		List<String> s = new ArrayList<String>();
		for (int i=0;i<150;i+=30) s.add(im.getStringStagiaire(pos).substring(i,i+30).trim());
		
		if ( s.get(0).length()+s.get(1).length()+s.get(2).length()+s.get(3).length()+s.get(4).length() !=0) {
		Stagiaire Stemp = new Stagiaire(s.get(0).toString(),s.get(1).toString(),s.get(2).toString(),s.get(3).toString(),s.get(4).toString());
		stagiaires.add(Stemp);
		}
	if (im.getBrancheDroit(pos) != -1) getAllNative(im.getBrancheDroit(pos),stagiaires);
		
		
	return stagiaires;

}
}
