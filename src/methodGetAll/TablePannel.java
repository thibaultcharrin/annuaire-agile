package methodGetAll;

import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class TablePannel{

	@SuppressWarnings("unchecked")
	public static TableView<Stagiaire> makeTableView(ObservableList<Stagiaire> stagiaires) {		

		TableView<Stagiaire> tp = new TableView<Stagiaire>(stagiaires);

		TableColumn<Stagiaire, String> colLastName = new TableColumn<Stagiaire, String>("Nom de famille");
		colLastName.setCellValueFactory(new PropertyValueFactory<Stagiaire, String>("lastName"));
		colLastName.setStyle("-fx-alignment: CENTER");

		TableColumn<Stagiaire, String> colFirstName = new TableColumn<Stagiaire, String>("Pr�nom");
		colFirstName.setCellValueFactory(new PropertyValueFactory<Stagiaire, String>("firstName"));
		colFirstName.setStyle("-fx-alignment: CENTER");

		TableColumn<Stagiaire, String> colDepartment = new TableColumn<Stagiaire, String>("D�partement");
		colDepartment.setCellValueFactory(new PropertyValueFactory<Stagiaire, String>("department"));
		colDepartment.setStyle("-fx-alignment: CENTER");

		TableColumn<Stagiaire, String> colPromoName = new TableColumn<Stagiaire, String>("Nom de la promotion");
		colPromoName.setCellValueFactory(new PropertyValueFactory<Stagiaire, String>("promoName"));
		colPromoName.setStyle("-fx-alignment: CENTER");

		TableColumn<Stagiaire, String> colYear = new TableColumn<Stagiaire, String>("Ann�e de promotion");
		colYear.setCellValueFactory(new PropertyValueFactory<Stagiaire, String>("year"));
		colYear.setStyle("-fx-alignment: CENTER");

		tp.getColumns().addAll(colLastName, colFirstName, colDepartment, colPromoName, colYear);
		tp.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		return tp;
	}		
}
