package exportAnnuairePdf;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import methodGetAll.Stagiaire;
import userInterface.MainApp;


public class  EventExportAnnuairePdf {
    
    public static void exportPdf (List<Stagiaire> lstagiaires){

    //Creating a PDF Document
    PDDocument document = new PDDocument();
    String timeNow = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss").format(Calendar.getInstance().getTime());
    String filename = MainApp.relativepath + "/Workspace_Projet1/Export/Export_Annuaire_EQL_" + timeNow + ".pdf";
    
    
    try {

        //Adding text in the form of string

        for (int j = 0; j < lstagiaires.size()/50+1; j++) {
            PDPage newPage = new PDPage();
            document.addPage(newPage);
            PDPageContentStream contentStream = new PDPageContentStream(document, newPage);
            //Begin the Content stream 
            contentStream.beginText();
            //Setting the font to the Content stream
            contentStream.setFont( PDType1Font.HELVETICA, 16 );
            //Setting the leading
            contentStream.setLeading(14.5f);
            //Setting the position for the line
            contentStream.newLineAtOffset(25, 725);
            for (int i = 50*j; i < 50*(j+1); i++) {
                if (i < lstagiaires.size()) {
                    try {
                        String text = lstagiaires.get(i).toString2();
                        //System.out.println(lstagiaires.get(i).toString2());
                        contentStream.showText(text);                       
                        contentStream.newLine();
                    } catch (IOException e1) {};
                
                }else break;
                
            }
            
            
            //Closing the content stream
            contentStream.endText();
            contentStream.close();
        }
        
        
        //Saving the document
        document.save(new File(filename));
        document.close();    
    
    } catch (IOException e1) {};
}
}