***** GESTIONNAIRE DE STAGIAIRES *****

|---------------------------------------------------------------------|
| Développé par Jean Bernard Heil, Simon Harnois, et Thibault Charrin |
|---------------------------------------------------------------------|

*** Documentation application JAVA SE annuaire EQL ***


	-Fenêtre initiale-
		
		A la première ouverture du programme, l'utilisateur est invité à placer un fichier 
		"stagiaires.txt", contenant l'ensemble de sa base de donnée, dans un dossier généré par le programme (la fenêtre s'ouvre automatiquement).
		NB: il est impératif que celui-ci soit formaté comme l'exemple fourni par l'organisme de l'EQL.
		Une fois placé, un mot de passe administrateur est demandé. L'Accès administrateur ouvre toutes les permissions pour manipuler la base de donnée.
		
		
	-Fenêtre principale-
		
		Cette fenêtre est constituée de deux éléments principaux : le panneau utilisateur (à gauche), et le panneau d'affichage (à droite).
		Le fichier importé précédement est par défaut trié par ordre alphabétique et affiché dans la table.
		La fenêtre principale aura par défaut, un accès utilisateur doté de plusieurs fonctions: 
		Exporter (au format PDF), Aide, Admin, Recherche, et Ajout.
		
	-AJOUT-
		 
		L'utilisateur doit remplir un ou plusieurs champ(s) correspondant à sa nouvelle entrée, puis valide en cliquant sur 'Ajout'.
		Un nouveau stagiaire sera donc ajouté dans la table de droite.
		 
	-RECHERCHE-
		
		L'utilisateur doit remplir un ou plusieurs champ(s) correspondant à sa recherche, ou séléctionner un des stagiaires en navigant dans la table de droite, 
		puis valider en cliquant sur 'Recherche'.
		Le résultat de sa recherche s'affiche donc dans la table de droite. Enfin, pour réinitialiser la table et les champs correspondants, l'utilisateur clique de nouveau sur 'Recherche'.
		
	-EXPORT-
		
		L'utilisateur peut exporter au format PDF un document issu de sa recherche en cliquant sur le bouton 'Export'. 
		Celui-ci sera placé dans un dossier généré automatiquement par le programme.
		
			Étape 1 - Pour exporter l'annuaire, il faut cliquer sur le bouton "Exporter".
			Étape 2 - Un fichier .pdf se créé alors dans le répertoire "C:\Users\formation\Desktop\Workspace_Projet1\ExportPDF"

	-AIDE-
	
		Affiche la documentation Help.txt.

	-ADMIN-
	
		L'utilisateur peut basculer à tout moment en accès administrateur qui ouvre de nouvelles fonctions à la fenêtre principale :
		Supprimer, et Mise-à-jour.
		Pour cela, l'utilisateur doit cliquer sur 'Admin' et renseigner le mot de passe qu'il aura défini à l'ouverture du programme.
		
			Étape 1 - Pour se connecter en tant qu'administrateur, il faut cliquer sur le bouton rouge "Log as Admin" à côté du bouton "Aide" (une fenêtre "Connexion en tant qu'administrateur" apparaît).
			Étape 2 - L'administrateur est alors invité à saisir le bon mot de passe pour se connecter. Si le mot de passe a été saisi correctement, alors la deuxième fenêtre se ferme, le panneau de gauche se colore en rouge et les boutons "Suppression" et "Mise-à-jour" sont rendus visibles. Sinon, un message d'erreur s'affiche et l'administrateur est invité à saisir de nouveau son mot de passe.
			Étape 3 - Il est ensuite possible de rebasculer en profil utilisateur en cliquant sur le bouton bleu "Log as User". Le panneau de gauche se colore alors en bleu.
			Étape 4 - Retour à l'étape 1 après être repassé en mode utilisateur.

	-MISE-A-JOUR-
		
		L'administrateur a la possibilité de mettre à jour un des champs qui figure dans la table. Pour cela, il peut séléctionner un des stagiaires
		en cliquant directement dans la table, ou à l'issue d'une recherche 'singulière' (qui ne retourne qu'un seul résultat).
		A la suite des modifications apportées au stagiaire, l'administrateur valide son entrée avec le bouton 'Mise-à-jour'.

	-SUPPRESSION-
	
		L'administrateur peut supprimer un par un chaque champ figurant dans la table. Pour cela, il séléctionne puis valide son entrée à l'aide du
		bouton 'Suppression'.
		
		
		